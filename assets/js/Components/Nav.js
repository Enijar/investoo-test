(() => {
    const nav = document.querySelector('.Nav');

    if (!nav) {
        return;
    }

    const state = {
        open: false
    };

    nav.querySelector('.Nav__mobile-toggle').addEventListener('click', () => {
        state.open = !state.open;

        if (state.open) {
            nav.classList.add('Nav--open');
        } else {
            nav.classList.remove('Nav--open');
        }
    });
})();
