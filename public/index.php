<?php

use Phalcon\Mvc\Application;

define('BASE_PATH', __DIR__ . '/..');
define('APP_PATH', BASE_PATH . '/app');

require_once BASE_PATH . '/vendor/autoload.php';
require_once APP_PATH . '/config/loader.php';
$di = require_once APP_PATH . '/config/services.php';

$app = new Application($di);

try {
    $response = $app->handle();
    $response->send();
} catch (\Exception $e) {
    // TODO: Remove in production
    echo 'Exception: ', $e->getMessage();
}
