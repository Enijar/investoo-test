<?php

namespace App\Investoo\Converters;

use Mimey\MimeTypes;
use SplFileObject;
use Ddeboer\DataImport\Reader\CsvReader;
use Ddeboer\DataImport\Writer\ExcelWriter;
use App\Investoo\Interfaces\Converter;

class Csv2Excel implements Converter
{
    /**
     * @param string $file
     * @return bool
     */
    public function validate($file)
    {
        // Check if file is a valid CSV file
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mimeType = finfo_file($finfo, $file);
        $mimes = new MimeTypes();
        $acceptedMimeTypes = $mimes->getAllMimeTypes('csv');
        return in_array($mimeType, $acceptedMimeTypes);
    }

    /**
     * @param string $file
     * @return string
     */
    public function convert($file)
    {
        $hash = time() . '-' . bin2hex(openssl_random_pseudo_bytes(16));

        $csv = new SplFileObject($file);
        $rows = new CsvReader($csv);
        $excel = new SplFileObject("uploads/{$hash}.xlsx", 'w');
        $writer = new ExcelWriter($excel);

        $writer->prepare();
        foreach ($rows as $row) {
            $writer->writeItem($row);
        }
        $writer->finish();

        return $hash;
    }
}
