<footer class="Footer menu align-justify">
    <div class="grid-container cell">
        <div class="grid-x">
            <div class="small-12 medium-8 large-9">
                <div class="grid-x align-justify">
                    <a href="#">Trading Courses</a>
                    <a href="#">Find a Broker</a>
                    <a href="#">Signals and Auto Trading</a>
                    <a href="#">About Us</a>
                    <a href="#">Contact</a>
                </div>

                <small class="grid-x">
                    <a href="#">Disclaimer</a>
                    <a href="#">Privacy Policy</a>
                    <a href="#">Refund Policy</a>
                    <a href="#">T&Cs</a>
                </small>

                <div class="Footer__country-select">
                    <img src="https://placehold.it/20x20/fff" class="img-rounded">
                    &nbsp;
                    English
                </div>
            </div>

            <div class="small-12 medium-4 large-3 grid-y align-spaced">
                <div class="grid-x align-right">
                    <img src="https://placehold.it/35x35/fff">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <img src="https://placehold.it/35x35/fff">
                </div>
                <p class="text-right">
                    Copyright <?php echo date('Y'); ?> investoo.com
                </p>
            </div>
        </div>
    </div>
</footer>
