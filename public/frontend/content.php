<main>
    <div class="grid-container">
        <div class="heading-title">
            <h6 class="text-turquoise">Forex</h6>
        </div>
    </div>

    <div class="Heading Heading--turquoise">
        <div class="grid-container">
            <div class="align-middle">
                <img src="https://placehold.it/20x20/fff">&nbsp;&nbsp;Home / Courses / Forex
            </div>

            <h1>Trading Courses</h1>

            <h6>Browse Courses</h6>
        </div>
    </div>

    <div class="grid-container">
        <div class="grid-x">
            <div class="small-12 medium-3">
                <h5 class="text-gray">Sort by</h5>
            </div>
        </div>
        <div class="grid-x">
            <div class="medium-12 large-2 panel-left">
                <div class="card card-rounded">
                    <div class="card-section">
                        <select title="Skill Level" name="skill_level">
                            <option value="">Skill Level</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                        <select title="Product Type" name="product_type">
                            <option value="">Product Type</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                        <select title="Course Type" name="course_type">
                            <option value="">Course Type</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                        <select title="Course Length" name="course_length">
                            <option value="">Course Length</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="medium-12 large-10">
                <div class="Cards grid-x">
                    <?php foreach (range(1, 16) as $course): ?>
                        <div class="Cards__card small-12 medium-6 large-3">
                            <div class="card card-rounded">
                                <img src="https://placehold.it/230x120">

                                <div class="card-section">
                                    <h5>Forex Beginners Course</h5>

                                    <small>20% Completed</small>
                                    <div class="progress">
                                        <div class="progress-meter background-turquoise" style="width: 20%"></div>
                                    </div>

                                    <small>User Rating</small>
                                    <div class="Rating grid-x align-middle">
                                        <div class="Rating__stars">
                                            <img src="../img/star.svg" class="Rating__stars-star">
                                            <img src="../img/star.svg" class="Rating__stars-star">
                                            <img src="../img/star.svg" class="Rating__stars-star">
                                            <img src="../img/star.svg" class="Rating__stars-star">
                                            <img src="../img/star.svg" class="Rating__stars-star">
                                        </div>
                                        <div class="Rating__score">5/5</div>
                                    </div>
                                </div>

                                <a href="#" class="Cards__card-button">
                                    <div class="Cards__card-button card-divider align-center background-turquoise">
                                        Start Free Course
                                    </div>
                                </a>
                            </div>

                            <div class="Cards__card-tooltip">
                                <h5>Forex Beginners Course</h5>

                                <br/>

                                <p class="text-gray">Modules: 2</p>
                                <p class="text-gray">Video Lessons: 10</p>

                                <br/>

                                <p>
                                    Introduction to the world of Forex trading markets, how to set up an account,
                                    terminology and how to prepare for your first trade.
                                </p>

                                <br/>

                                <div class="cell grid-x align-center">
                                    <a href="#" class="Button Button--primary">Free Access</a>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>

                    <div class="Cards__pagination cell">
                        <div class="grid-x align-justify align-middle">
                            <div class="text-gray">
                                Showing 1-16 of 100 results
                            </div>

                            <div class="text-gray">
                                &laquo; <strong>&lsaquo; Page 3 &rsaquo;</strong> &raquo;
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
