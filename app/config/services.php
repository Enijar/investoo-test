<?php

use Phalcon\Mvc\View;
use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Url as UrlProvider;
use Phalcon\Db\Adapter\Pdo\Mysql as PdoMysql;

$di = new FactoryDefault();

$di->set('view', function () {
    $view = new View();
    $view->setViewsDir(APP_PATH . '/views');
    return $view;
});

$di->set('url', function () {
    $url = new UrlProvider();
    $url->setBaseUri('/');
    return $url;
});

$di->set('db', function () {
    return new PdoMysql([
        'host' => getenv('DB_HOST'),
        'username' => getenv('DB_USERNAME'),
        'password' => getenv('DB_PASSWORD'),
        'dbname' => getenv('DB_DATABASE'),
    ]);
});

$di->set('router', function () {
    return require_once APP_PATH . '/routes.php';
});

return $di;
