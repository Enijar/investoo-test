<?php

use Phalcon\Loader;

$loader = new Loader();

$loader->registerDirs([
    APP_PATH . '/controllers',
    APP_PATH . '/models',
]);

$loader->register();

$dotenv = new Dotenv\Dotenv(BASE_PATH);
$dotenv->load();
