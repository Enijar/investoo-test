<?php

namespace App\Investoo;

use App\Investoo\Converters\Csv2Excel;
use Mimey\MimeTypes;
use Phalcon\Http\Request\File;

class Conversion
{
    /**
     * Check that the uploaded file can be converted to
     * the requested conversion type.
     *
     * @param File $file
     * @param string $requestedConversion
     * @return bool
     */
    public function canConvert(File $file, $requestedConversion)
    {
        $converters = $this->getSupportedConversions();

        if (!isset($converters[$requestedConversion])) {
            return false;
        }

        $supportedTypes = array_keys($converters[$requestedConversion]);
        $type = $this->getFileType($file);
        return in_array($type, $supportedTypes);
    }

    /**
     * Return an instance of the requested conversion
     * type for the file.
     *
     * @param File $file
     * @param string $requestedConversion
     * @return mixed
     */
    public function getConverterInstance(File $file, $requestedConversion)
    {
        $converters = $this->getSupportedConversions();
        $type = $this->getFileType($file);
        return new $converters[$requestedConversion][$type]();
    }

    /**
     * Get the type of the file from the extension.
     *
     * @param File $file
     * @return string
     */
    private function getFileType(File $file)
    {
        $mimes = new MimeTypes();
        return $mimes->getExtension($file->getType());
    }

    /**
     * Array of all the supported conversion types.
     *
     * @return array
     */
    private function getSupportedConversions()
    {
        return [
            'xlsx' => [
                'csv' => Csv2Excel::class
            ]
        ];
    }
}
