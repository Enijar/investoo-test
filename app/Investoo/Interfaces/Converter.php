<?php

namespace App\Investoo\Interfaces;

interface Converter
{
    /**
     * @param string $file
     * @return bool
     */
    public function validate($file);

    /**
     * @param string $file
     * @return string
     */
    public function convert($file);
}
