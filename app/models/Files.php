<?php

use Phalcon\Mvc\Model;

class Files extends Model
{
    protected $id;

    protected $hash;

    protected $name;

    protected $type;

    protected $created_at;

    private static $columns = ['name', 'hash', 'type', 'created_at'];

    /**
     * Get all files.
     *
     * @return array
     */
    public static function all()
    {
        $files = self::find(['columns' => implode(',', self::$columns)])->toArray();
        foreach ($files as $index => $file) {
            $file['download_link'] = "uploads/{$file['hash']}.{$file['type']}";
            $files[$index] = $file;
        }
        return $files;
    }

    /**
     * Get a file with the given hash.
     *
     * @param string $hash
     * @return null
     */
    public static function getByHash($hash)
    {
        $files = Files::query()
            ->columns(self::$columns)
            ->where('hash = :hash:')
            ->bind(compact('hash'))
            ->execute();
        return count($files) === 0 ? null : $files[0];
    }
}
