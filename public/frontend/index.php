<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="author" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <title>Test</title>

        <link rel="stylesheet" href="../css/app.css">
    </head>

    <body>
        <?php include_once __DIR__ . '/nav.php'; ?>
        <?php include_once __DIR__ . '/content.php'; ?>
        <?php include_once __DIR__ . '/footer.php'; ?>

        <script src="../js/app.js"></script>
    </body>
</html>
