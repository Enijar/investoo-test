const elixir = require('laravel-elixir');
require('laravel-elixir-browserify-official');

elixir.config.assetsPath = 'assets';

elixir(mix => {
    mix.sass('app.scss');
    mix.browserify('app.js');
});
