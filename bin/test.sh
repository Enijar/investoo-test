#!/usr/bin/env bash

URL=http://localhost/
FILE=$(pwd)/data/test.csv

# Check if dependencies exist
dependencies=(python curl)
for dependency in "${dependencies[@]}"
do
    if command -v "$dependency" > /dev/null
    then
        continue
    fi

    # Tell the user that the dependency is not installed, then exit the script
    echo "Dependency $dependency isn't installed. Install this dependency to continue."
    exit 0
done

# Should upload the test file to the uploads
# directory and save to the database.
curl -X POST ${URL} -H 'content-type: multipart/form-data' -F conversion=xlsx -F file=@${FILE} 2>/dev/null

# Should return all data stored in the database
curl -X GET ${URL} -H 'content-type: multipart/form-data' 2>/dev/null | python -m json.tool
