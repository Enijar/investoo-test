# Investoo Test

The purpose of this test was to build an API, based on the Phalcon PHP framework. The API required the following features:

- Endpoint to accept a CSV file, convert it into an Excel file, then save the info to a database
- Endpoint to list all previously saved files
- Endpoint to view a single file, with a link to download the file 

### API Setup & Installation

Install PHP dependencies

```
$ composer install
```

Setup environment variables

```
$ cp .env.example .env
```

Then modify the database host, username, password, and database name.

Create the database. This must be the same name as that of DB_DATABASE inside the .env file.

Once the database is created, run the SQL migration inside of the data/migration.sql file to construct the files table.

Check that everything is working bu running

```
$ ./bin/test.sh
```

This script will call the API endpoints to create an Excel file, using the test CSV file inside the data directory, 
and then show the created records.
 
### Frontend Setup & Installation

Install JS dependencies

```
$ npm install
``` 

Compile assets

```
$ npm run build
``` 

Visit http://localhost/frontend to see the frontend.
