<nav class="Nav menu align-justify">
    <div class="Nav__mobile-toggle">
        <div></div>
        <div></div>
        <div></div>
    </div>

    <div class="Nav__item grid-x align-bottom">
        <a href="#">
            <img src="https://placehold.it/200x35/51c3aa/fff">
        </a>
        <a href="#">Courses</a>
        <a href="#">Brokers</a>
        <a href="#">Signals</a>
        <a href="#">Trading Room</a>
    </div>

    <div class="Nav__item grid-x align-middle">
        <a href="#" class="Button Button--rounded Button--bordered">Log In</a>
        <a href="#" class="Button Button--primary">Sign Up</a>
        <a href="#" class="Button Button--rounded Button--bordered Button--pro">
            <img src="https://placehold.it/185x15/f5a524/fff">
        </a>
        <a href="#">
            <img src="https://placehold.it/38x38/e5c1ae/fff" class="img-rounded">
        </a>
    </div>
</nav>
