<?php

use App\Investoo\Conversion;
use App\Investoo\Response;

class ApiController extends BaseController
{
    /**
     * Show a specific file, if a hash was sent in the request,
     * else send all the files as a response.
     */
    public function indexAction()
    {
        $params = $this->router->getParams();
        $hash = isset($params['hash']) ? $params['hash'] : null;
        $hash = strlen(trim($hash)) === 0 ? null : trim($hash);

        // If there is no hash parameter, then show all files.
        if (is_null($hash)) {
            Response::json(['status' => 'OK', 'data' => Files::all()], 200);
        }

        // Find file with the given hash
        $file = Files::getByHash($hash);

        // Check the file exists.
        if (is_null($file)) {
            Response::json(['status' => 'ERROR', 'error' => 'File not found.'], 404);
        }

        Response::json(['status' => 'OK', 'data' => $file], 200);
    }

    /**
     * Convert a file into the requested conversion format,
     * save it to the local dist, then store details in database.
     */
    public function createAction()
    {
        // Check that request has files
        if (!$this->request->hasFiles()) {
            Response::json(['status' => 'ERROR', 'error' => 'No file provided.'], 400);
        }

        // Check that request has conversion
        if (!$this->request->has('conversion')) {
            Response::json(['status' => 'ERROR', 'error' => 'No conversion type provided.'], 400);
        }

        $requestedConversion = $this->request->get('conversion');
        $file = $this->request->getUploadedFiles()[0];
        $conversion = new Conversion();

        // Check that the file can be converted to the requested conversion.
        if (!$conversion->canConvert($file, $requestedConversion)) {
            Response::json(['status' => 'ERROR', 'error' => "Cannot convert file to {$requestedConversion}."], 400);
        }

        $name = $file->getName();
        $filePath = $file->getTempName();
        $converter = $conversion->getConverterInstance($file, $requestedConversion);

        // Check that the file is valid for the converter.
        if (!$converter->validate($filePath)) {
            Response::json(['status' => 'ERROR', 'error' => 'Invalid CSV file.'], 400);
        }

        // Convert, save and return a response.
        $hash = $converter->convert($filePath);
        $type = $requestedConversion;
        $data = compact('hash', 'name', 'type');
        $file = new Files();
        $saved = $file->save($data);
        $data['download_link'] = "uploads/{$hash}.{$type}";

        if (!$saved) {
            Response::json(['status' => 'ERROR', 'error' => 'File not saved to the DB.'], 400);
        }

        Response::json(['status' => 'OK', 'data' => (object)$data], 200);
    }
}
