<?php

namespace App\Investoo;

class Response
{
    /**
     * @param array $content
     * @param int $status
     */
    public static function json($content = [], $status = 200)
    {
        $response = new \Phalcon\Http\Response();
        $response->setContentType('application/json');
        $response->setStatusCode($status);
        $response->setJsonContent($content);
        $response->send();
        exit;
    }
}
