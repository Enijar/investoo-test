<?php

use Phalcon\Mvc\Router;

$router = new Router();

$router->addGet('/{hash}', [
    'controller' => 'Api',
    'action' => 'index',
]);

$router->addPost('/', [
    'controller' => 'Api',
    'action' => 'create',
]);

return $router;
